

const cursorInner = document.querySelector("[data-cursor-inner]");
const cursorOuter = document.querySelector("[data-cursor-outer]");

window.addEventListener("mousemove", function (e) {
    const posX = e.clientX;
    const posY = e.clientY;

    cursorInner.style.left = `${posX}px`;
    cursorInner.style.top = `${posY}px`;

    //cursorOuter.style.left = `${posX}px`;
    //cursorOuter.style.top = `${posY}px`;

    cursorOuter.animate({
        left: `${posX}px`,
        top: `${posY}px`
    }, {duration: 300, fill: "forwards" });
});


